# vim-cmakeist

Vim plugin to make working with CMake easy as a breeze.

## License

Copyright (c) Jens Carl. Distributed under the same terms as Vim itself.
See `:help license`
