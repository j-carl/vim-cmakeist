" vim-cmakeist - support for CMake
" Maintainer:   Jens Carl <j dot carl43 at gmx dot de>
" Version:      0.0.1

if exists('g:loaded_cmakeist')
    finish
endif

let g:loaded_cmakeist = 1

if !exists('g:cmakeist_cmake_executable')
    let g:cmakeist_cmake_executable = "cmake"
endif

if !exists('g:cmakeist_cmake_builddir')
  let g:cmakeist_cmake_builddir = "build"
endif

if !executable(g:cmakeist_cmake_executable)
    echoerr "vim-cmakeist requires CMake. Please install it and find it via PATH or set g:cmakeist_cmake_executable"
    finish
endif

" escape file name (Thanks to Time Pope and https://github.com/tpope/vim-fugitive.git)
function! s:fnameescape(file) abort
  if exists('*fnameescape')
    return fnameescape(a:file)
  else
    return escape(a:file, " \t\n*?[{`$\\%#'\"|!<")
  endif
endfunction

function! s:CMakeistSetup(...) abort

  let l:builddir = s:fnameescape(g:cmakeist_cmake_builddir)
  let &makeprg = g:cmakeist_cmake_executable . ' --build ' . l:builddir
endfunction

command! -nargs=? -bar CMakeSetup call s:CMakeistSetup(<f-args>)

" vim:set sw=2 ts=2:
